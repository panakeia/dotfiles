# ZSH config directory
export ZDOTDIR="$HOME/.config/zsh"

# XDG config
export XDG_CONFIG_HOME="$HOME/.config"

# General configuration
export TERM=xterm-256color
export EDITOR='vim'
export VISUAL='vim'
export PATH="$HOME/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/bin"
export MANPATH="/usr/local/man:$MANPATH"
