```bash
git clone --separate-git-dir="$HOME/.config/dotfiles" /path/to/repo "$HOME/dotfiles-tmp"
cp ~/dotfiles-tmp/.gitmodules ~  # If you use Git submodules
rm -r ~/dotfiles-tmp/
alias config='/usr/bin/git --git-dir="$HOME/.config/dotfiles" --work-tree="$HOME"'
```