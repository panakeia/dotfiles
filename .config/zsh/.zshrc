# History settings
HISTFILE="$ZDOTDIR/.zsh_history"
HISTSIZE=1000
SAVEHIST=1000
REPORTTIME=10

# Aliases
alias ..='cd ..'
alias la='ls -a'
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'

# ZSH options
setopt NO_BG_NICE
setopt NO_HUP
setopt NO_LIST_BEEP
setopt PROMPT_SUBST

setopt EXTENDED_HISTORY
setopt APPEND_HISTORY
setopt INC_APPEND_HISTORY SHARE_HISTORY
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_REDUCE_BLANKS

bindkey -e

# ZSH completion settings
zstyle :compinstall filename "$HOME/pan/.zsh/.zshrc"
autoload -Uz compinit
compinit -D

export PROMPT='%1~ $ '
