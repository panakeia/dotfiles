# default foreground color (%N) - -1 is the "default terminal color"
default_color = "-1";

# print timestamp/servertag at the end of line, not at beginning
info_eol = "false";

# these characters are automatically replaced with specified color
replaces = { "[]=" = "%K$*%n"; };

abstracts = {
  ##
  ## generic
  ##

  indent_default = "";

  # text to insert at the beginning of each non-message line
  line_start = "";

  # status line styling
  status = "%w$*%n";

  # status highlight styling
  status_hilight = "%N$*%n";

  # timestamp styling
  # timestamp = "%K$*%n";
  timestamp = "";

  # any kind of text that needs hilighting
  hilight = "%_$*%_";

  # any kind of error message
  error = "%R$*%n";

  # channel name is printed
  channel = "%_$*%_";

  # nick is printed
  nick = "%_$*%_";

  # nick host is printed
  nickhost = "[$*]";

  # server name is printed
  server = "%_$*%_";

  # some kind of comment is printed
  comment = "%K(%n$*%K)%n";

  # reason for something is printed (part, quit, kick, ..)
  reason = "{comment $*}";

  # mode change is printed ([+o nick])
  mode = "{comment $*}";

  ##
  ## channel specific messages
  ##

  # highlighted nick/host is printed (joins)
  channick_hilight = "%C$*%n";
  chanhost_hilight = "{nickhost %c$*%n}";

  # nick/host is printed (parts, quits, etc.)
  channick = "%c$*%n";
  chanhost = "{nickhost $*}";

  # highlighted channel name is printed
  channelhilight = "%c$*%n";

  # ban/ban exception/invite list mask is printed
  ban = "%c$*%n";

  ##
  ## messages
  ##

  # the basic styling of how to print message, $0 = nick mode, $1 = nick
  # msgnick = "%K$0%n$1-%K -%n %|";
  msgnick = "%K$0$1%K %w- %n%|";

  # message from you is printed. "msgownnick" specifies the styling of the
  # nick ($0 part in msgnick) and "ownmsgnick" specifies the styling of the
  # whole line.

  # $0 = nick mode, $1 = nick
  ownmsgnick = "{msgnick $0 $1-}";
  ownnick = "%w$*%n";

  # public message in channel, $0 = nick mode, $1 = nick
  pubmsgnick = "{msgnick $0 $1-}";
  pubnick = "%N$*%n";

  # public message in channel meant for me, $0 = nick mode, $1 = nick
  pubmsgmenick = "{msgnick $0 $1-}";
  menick = "%Y$*%n";

  # public highlighted message in channel
  # $0 = highlight color, $1 = nick mode, $2 = nick
  pubmsghinick = "{msgnick $1 $0$2-%n}";

  # channel name is printed with message
  msgchannel = "%K:%c$*%n";

  # private message, $0 = nick, $1 = host
  privmsg = "[%R$0%K(%r$1-%K)%n] ";

  # private message from you, $0 = "msg", $1 = target nick
  ownprivmsg = "[%r$0%K(%R$1-%K)%n] ";

  # own private message in query
  ownprivmsgnick = "{msgnick  $*}";
  ownprivnick = "%_$*%n";

  # private message in query
  privmsgnick = "{msgnick  %R$*%n}";

  ##
  ## Actions (/ME stuff)
  ##

  # used internally by this theme
  # action_core = "$*";
  action_core = "%K*\011\011%N$*%n";

  # generic one that's used by most actions
  action = "{action_core $*} ";

  # own action, both private/public
  ownaction = "%w{action $*}%n";

  # own action with target, both private/public
  ownaction_target = "{action_core $0}%K:%c$1%n ";

  # private action sent by others
  pvtaction = "%_ (*) $*%n ";
  pvtaction_query = "{action $*}";

  # public action sent by others
  pubaction = "{action $*}";


  ##
  ## other IRC events
  ##

  # whois
  whois = "%# $[8]0 : $1-";

  # notices
  ownnotice = "%w$N %Kto%n $0%K:%n %|";
  notice = "$* %|";
  pubnotice_channel = "%K:%m$*";
  pvtnotice_host = "%K(%m$*%K)";
  servernotice = "%g!$*%n ";

  # CTCPs
  ownctcp = "%w$N %K$1 to %n$0%K:%n %|";
  ctcp = "%g$*%n";

  # wallops
  wallop = "%_$*%n: ";
  wallop_nick = "%n$*";
  wallop_action = "%_ * $*%n ";

  # netsplits
  netsplit = "%R$*%n";
  netjoin = "%C$*%n";

  # /names list
  names_prefix = "";
  names_nick = "%K$0%n$1- ";
  names_nick_op = "{names_nick $*}";
  names_nick_halfop = "{names_nick $*}";
  names_nick_voice = "{names_nick $*}";
  names_users = "[%g$*%n]";
  names_channel = "%G$*%n";

  # DCC
  dcc = "%g$*%n";
  dccfile = "%_$*%_";

  # DCC chat, own msg/action
  dccownmsg = "[%r$0%K($1-%K)%n] ";
  dccownnick = "%R$*%n";
  dccownquerynick = "%_$*%n";
  dccownaction = "{action $*}";
  dccownaction_target = "{action_core $0}%K:%c$1%n ";

  # DCC chat, others
  dccmsg = "[%G$1-%K(%g$0%K)%n] ";
  dccquerynick = "%G$*%n";
  dccaction = "%_ (*dcc*) $*%n %|";

  ##
  ## statusbar
  ##

  # default background for all statusbars. You can also give
  # the default foreground color for statusbar items.
  # sb_background = "%4%B";
  sb_background = "%4%B";

  # default backround for "default" statusbar group
  #sb_default_bg = "%4";
  # background for prompt / input line
  sb_prompt_bg = "%n";
  # background for info statusbar
  # sb_info_bg = "%8";
  sb_info_bg = "%8";
  # background for topicbar (same default)
  #sb_topic_bg = "%4";

  # text at the beginning of statusbars. sb-item already puts
  # space there, so we don't use anything by default.
  sbstart = "";
  # text at the end of statusbars. Use space so that it's never
  # used for anything.
  sbend = " ";

  topicsbstart = "{sbstart $*}";
  topicsbend = "{sbend $*}";

  prompt = "%b$C$Q %r>%n ";

  sb = " %c[%n$*%c]%n";
  sbstatus = "%b$*%n";
  sbmode = "{sbstatus (+$*)}";
  sbaway = "{sbstatus (%B$*%b) }";
  sbchan = "{sbstatus in }$*";
  sbquery = "{sbstatus with }$* {sbstatus on} {sbservertag $tag}";
  sbservertag = "$0";
  sbnickmode = "$0";

  # activity in statusbar

  # ',' separator
  sb_act_sep = " ";
  # normal text
  sb_act_text = "%b$*";
  # public message
  sb_act_msg = "%B$*";
  # hilight
  sb_act_hilight = "%B$*";
  # hilight with specified color, $0 = color, $1 = text
  sb_act_hilight_color = "%B$1-%n";
};

formats = {
  "fe-common/core" = {
    servertag = "%B$0%n{status :} %|";
    kick = "";
    part = "";
    quit = "";

    cant_connect = "Unable to connect to $0 ($2)";
    connecting = "Connecting to $1:$2";
    connection_established = "Connected";
    connection_lost = "Connection lost";
    daychange = "";
    endofnames = "{status {status_hilight $1} nicks {comment {status_hilight $2} ops, {status_hilight $3} halfops, {status_hilight $4} voices, and {status_hilight $5} normal}}";
    join = "{status $0 joined}";
    lag_disconnected = "No reply in $1 seconds, disconnecting";
    line_start_irssi = "";
    looking_up = "";
    names = "{status —————} Nicks {status —————————————————————————————————————————————————————————————————————————}";
    new_topic = "{status $0 set the topic to:} $2";
    nick_changed = "{status $0 is now known as $1}";
    pubmsg_hilight = "{pubmsghinick $0 $3$nickalign $nickcolor$1$nicktrunc}$2";
    query_start = "";
    reconnecting = "Reconnecting to $1:$2";
    server_changed = "";
    server_reconnect_removed = "Removed reconnection to $0";
    topic_unset = "{status $0 unset the topic}";
    your_nick_changed = "{status You're now known as $1}";
    own_msg = "{ownmsgnick $2$nickalign {ownnick $nickcolor$0$nicktrunc}}$1";
    own_msg_channel = "{ownmsgnick $3$nickalign {ownnick $nickcolor$0$nicktrunc}{msgchannel $1}}$2";
    own_msg_private_query = "{ownprivmsgnick $nickalign{ownprivnick $nickcolor$2$nicktrunc}}$1";
    pubmsg_me = "{pubmsgmenick $2$nickalign {menick $nickcolor$0$nicktrunc}}$1";
    pubmsg_me_channel = "{pubmsgmenick $3$nickalign {menick $nickcolor$0$nicktrunc}{msgchannel $1}}$2";
    pubmsg_hilight_channel = "{pubmsghinick $0 $4$nickalign $nickcolor$1$nicktrunc{msgchannel $2}}$3";
    pubmsg = "{pubmsgnick $2 {pubnick \00303$0}}$1";
    pubmsg_channel = "{pubmsgnick $3$nickalign {pubnick $nickcolor$0$nicktrunc}{msgchannel $1}}$2";
    msg_private_query = "{privmsgnick $nickalign$nickcolor$0$nicktrunc}$2";
  };

  "fe-common/irc" = {
    action_public = "{pubaction $nickalign$0$nicktrunc}$1";
    action_private_query = "{pvtaction_query $nickalign$0$nicktrunc}$2";
    ctcp_ping_reply = "";
    ctcp_reply = "";
    ctcp_requested = "";
    ctcp_requested_unknown = "";
    notice_private = "{notice %c$0%n{status :}}$2";
    own_ctcp = "{ownctcp %M$0%n $1}$2";
    own_notice = "";

    banlist_long = "$[-2]0{status :} %|{ban $2} {status by} $3";
    chanmode_change = "{status $2 set mode $1}";
    channel_created = "";
    channel_mode = "";
    channel_synced = "";
    end_of_who = "";
    end_of_whois = "";
    end_of_whowas = "";
    netsplit = "{status Netsplit quits: %|$2}";
    netsplit_join = "{status Netsplit joins: %|$0}";
    netsplit_join_more = "{status Netsplit joins: %|$0, and $1 more}";
    netsplit_more = "{status Netsplit quits: %|$2and $3 more}";
    nick_away = "= {status $0 is away {reason $1}}";
    no_topic = "{status No topic set}";
    server_chanmode_change = "{status $2 set mode $1}";
    topic = "{status Topic:} %|$1";
    topic_info = "";
    usermode_change = "Server set mode $0 $1";
    who = "%#{channelhilight $0}{status :} %|$1 {comment {status $4@$5}}";
    whois = "$0 {status {comment $1@$2}}%:{whois ircname $3}";
    whois_idle_signon = "{whois idle %|$1 days $2 hours $3 mins $4 secs {status {comment signon: $5}}}";
    whois_server = "{whois server %|$1 {status {comment $2}}}";
    whois_special = "           %|$1";
    whowas = "$0 {status {comment $1@$2}}%:{whois ircname $3}";
    your_nick_owned = "Your nick is owned by $3 {comment {status $1@$2}}";
    own_action = "{ownaction $nickalign$0$nicktrunc}$1";
    action_private = "{pvtaction $nickalign$0$nicktrunc}$2";
  };

  "fe-text" = {
    lastlog_start = "{status ╍╍╍╍╍} Lastlog {status ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍}";
    lastlog_end = "{status ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍}";
  };
};
