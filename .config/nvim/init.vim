set nocompatible
filetype plugin indent on
syntax on
set t_Co=16

set title
set titlestring=\vim\ -\ %t titlelen=70 

set viminfo+=n~/.vim/viminfo

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'

Plugin 'tpope/vim-sensible'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-characterize'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-unimpaired'
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/syntastic'
Plugin 'junegunn/goyo'
Plugin 'junegunn/limelight'
Plugin 'junegunn/fzf'
Plugin 'janko-m/vim-test'
Plugin 'wellle/targets.vim'

Plugin 'hachy/eva01.vim'
Plugin 'dracula/vim'

call vundle#end()
